-- ----------------------------------------------------------------
--          Indyco Builder - Auto-Generated DDL Script           --
-- ----------------------------------------------------------------
--
-- Project Name . . . . . . : Multidimensional Modeling
--  + Exported Datamarts. . : Datamart
--  + Export Timestamp. . . : 2019-10-24 04:09:37
--
-- Selected Profile . . . . : Star Schema
--  + Shared Hierarchies. . : Snowflake
--  + Cross Dimensional Attr: Bridge table without surrogates
--  + Degenerate Dimensions : Use fact table
--  + Multiple Arcs . . . . : Bridge table without surrogates
--  + Recursions. . . . . . : Use self-referencing keys
--
-- Target DB. . . . . . . . : Oracle Database
--  + Export Primary Keys . : True
--  + Export Foreign Keys . : True
--  + Nullable columns. . . : True
--
-- ----------------------------------------------------------------

CREATE TABLE "DT_TIME"
(
  "ID_TIME" NUMBER (9),
  "TIME" DATE
);
ALTER TABLE "DT_TIME" ADD CONSTRAINT "PK_DT_TIME" PRIMARY KEY ("ID_TIME");





CREATE TABLE "DT_AIRCRAFT"
(
  "ID_AIRCRAFT" NUMBER (9),
  "AIRCRAFT" VARCHAR2 (255 CHAR),
  "MODEL" VARCHAR2 (255 CHAR),
  "ID_MODEL" NUMBER (9),
  "ID" VARCHAR2 (255 CHAR),
  "ID_ID" NUMBER (9)
);
ALTER TABLE "DT_AIRCRAFT" ADD CONSTRAINT "PK_DT_AIRCRAFT" PRIMARY KEY ("ID_AIRCRAFT");





CREATE TABLE "FT_STATE"
(
  "ID_TIME" NUMBER (9),
  "ID_AIRCRAFT" NUMBER (9),
  "ID_LOGBOOK_ENTRY" NUMBER (9),
  "DELAY_MINUTES" NUMBER (9),
  "DURATION_HOURS" NUMBER (9),
  "IS_SCHEDULED" NUMBER (1),
  "IS_FLIGHT" NUMBER (1),
  "IS_MAINTENANCE" NUMBER (1),
  "IS_READY" NUMBER (1),
  "IS_CANCELLED" NUMBER (1)
);
ALTER TABLE "FT_STATE" ADD CONSTRAINT "PK_FT_STATE" PRIMARY KEY ("ID_TIME", "ID_AIRCRAFT", "ID_LOGBOOK_ENTRY");





CREATE TABLE "DT_AIRCRAFT_2"
(
  "ID_AIRCRAFT" NUMBER (9),
  "AIRCRAFT" VARCHAR2 (255 CHAR),
  "MODEL" VARCHAR2 (255 CHAR),
  "MANUFACTURER" VARCHAR2 (255 CHAR)
);
ALTER TABLE "DT_AIRCRAFT_2" ADD CONSTRAINT "PK_DT_AIRCRAFT_2" PRIMARY KEY ("ID_AIRCRAFT");





CREATE TABLE "DT_LOGBOOK_ENTRY"
(
  "ID_LOGBOOK_ENTRY" NUMBER (9),
  "LOGBOOK_ENTRY" VARCHAR2 (255 CHAR),
  "REPORTER" VARCHAR2 (255 CHAR),
  "IDENTIFIER" VARCHAR2 (255 CHAR),
  "AIRPORT_CODE" VARCHAR2 (255 CHAR)
);
ALTER TABLE "DT_LOGBOOK_ENTRY" ADD CONSTRAINT "PK_DT_LOGBOOK_ENTRY" PRIMARY KEY ("ID_LOGBOOK_ENTRY");








ALTER TABLE "FT_STATE" ADD CONSTRAINT "FK_FT_STATE_ID_TIME_ID_TIME" FOREIGN KEY ("ID_TIME") REFERENCES "DT_TIME" ("ID_TIME");
ALTER TABLE "FT_STATE" ADD CONSTRAINT "FK_FT_STATE_ID_ARCR_ID_ARCR" FOREIGN KEY ("ID_AIRCRAFT") REFERENCES "DT_AIRCRAFT_2" ("ID_AIRCRAFT");
ALTER TABLE "FT_STATE" ADD CONSTRAINT "FK_FT_STT_ID_LGBK_ENTR_ID_LGB" FOREIGN KEY ("ID_LOGBOOK_ENTRY") REFERENCES "DT_LOGBOOK_ENTRY" ("ID_LOGBOOK_ENTRY");




-- DROP TABLE "DT_TIME" CASCADE CONSTRAINTS;
-- DROP TABLE "DT_AIRCRAFT" CASCADE CONSTRAINTS;
-- DROP TABLE "FT_STATE" CASCADE CONSTRAINTS;
-- DROP TABLE "DT_AIRCRAFT_2" CASCADE CONSTRAINTS;
-- DROP TABLE "DT_LOGBOOK_ENTRY" CASCADE CONSTRAINTS;




